# Go Protobuf

A demo project fo rlearning how to build a go project. To build this, clone it
to `$HOME/go/src/`, or clone it elsewhere and add it to the `GOPATH`.

The `Makefile` should get all of the dependencies needed for the project
automatically, but I haven't understood exactly how the dependencies are managed
to know if that is true for sure. Next step will be to contrainerize a
reproducible build system so I can learn for sure.
