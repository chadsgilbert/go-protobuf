
.PHONY: all
all: add_person list_people

addressbook.pb.go: addressbook/addressbook.proto
	protoc -I=addressbook --go_out=addressbook/ addressbook.proto

add_person: addressbook.pb.go add_person.go
	go build -o add_person add_person.go 

list_people: addressbook.pb.go list_people.go
	go build -o list_people list_people.go

clean:
	rm addressbook.pb.go

install:
	go install
