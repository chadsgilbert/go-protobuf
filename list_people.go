package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"github.com/golang/protobuf/proto"
	"go-protobuf/addressbook"
)

func writePerson(w io.Writer, p *addressbook.Person) {
	fmt.Fprintln(w, "Person ID:", p.Id)
	fmt.Fprintln(w, "     Name:", p.Name)
	if p.Email != "" {
		fmt.Fprintln(w, " Email address:", p.Email)
	}

	for _, pn := range p.Phones {
		switch pn.Type {
		case addressbook.Person_MOBILE:
			fmt.Fprint(w, " Mobile phone #:")
		case addressbook.Person_HOME:
			fmt.Fprint(w, "   Home phone #:")
		case addressbook.Person_WORK:
			fmt.Fprint(w, "   Work phone $:")
		}
		fmt.Fprintln(w, pn.Number)
	}
}

func listPeople(w io.Writer, book *addressbook.AddressBook) {
	for _, p := range book.People {
		writePerson(w, p)
	}
}

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("Usage:  %s ADDRESS_BOOK_FILE\n", os.Args[0])
	}
	fname := os.Args[1]

	in, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Fatalln("Error reading file:", err)
	}
	book := &addressbook.AddressBook{}
	if err := proto.Unmarshal(in, book); err != nil {
		log.Fatalln("Failed to parse address book:", err)
	}

	listPeople(os.Stdout, book)
}

